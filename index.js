const axios = require("axios");
const admin = require("firebase-admin");
const serviceAccount = require("./at202xcloud-e8a6266f9a57.json");
admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });
const db = admin.firestore();

exports.run = (req, res) => {
  let tradeID = req.body.tradeID;
  let url =
    "https://api-fxpractice.oanda.com/v3/accounts/101-004-10015616-001/trades/" +
    tradeID;
  console.log(tradeID, "tradeID");
  const options = {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
    },
  };
  axios
    .get(url, options)

    .then(function (response) {
      let trade = response.data.trade;
      console.log(trade, "tradeState");
      res.send(trade);
    })
    .catch(function (error) {
      console.log(error.message);
    });
};
